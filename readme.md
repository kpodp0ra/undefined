# Undefined

Undefined is undefined. Thats all.

# Casing ?

Because it is a module you can use it lowercase:

```
import undefined
```

Because it looks more like a keyword (`None`, `True`, `False`), you can use it upper case:

```
import undefined as Undefined
```

or

```
from undefined import Undefined
```

I tend to be torn between lowercase, for simplicity, and Uppercase.


# Why not `None`, difference with `None`

`undefined` is likely slower, and as it is a regular Python object there are a few  on purpose (or not difference).

### Unlike `None`, you can assign to it

```
>>> None = 3 
SyntaxError: can't assign to keyword
```

```
>>> undefined = 3
>>> undefned
3
```

### String form

`str(undefined)` raises. `repr(undefined)` is the unicode string `'Undefined'`
